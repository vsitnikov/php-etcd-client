<?php

namespace vsitnikov\Etcd\Exceptions;

use Exception;

/**
 * Class EtcdException
 *
 * @package vsitnikov\Etcd\Exceptions
 */
class EtcdException extends Exception
{
}
