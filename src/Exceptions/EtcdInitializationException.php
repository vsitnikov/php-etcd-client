<?php

namespace vsitnikov\Etcd\Exceptions;

/**
 * Class EtcdException
 *
 * @package vsitnikov\Etcd\Exceptions
 */
class EtcdInitializationException extends EtcdException
{
}
