<?php declare(strict_types=1);
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace vsitnikov\Etcd;

use GuzzleHttp\Client;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\GuzzleException;

use vsitnikov\Etcd\Exceptions\EtcdInitializationException;

/**
 * ETCD class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\Etcd
 * @copyright Copyright (c) 2019
 */
abstract class AbstractEtcdClient
{
    public const HTTP_TIMEOUT = 30;
    
    public const RESPONSE_NONE = 0x00;
    public const RESPONSE_RAW = 0x01;
    public const RESPONSE_PAYLOAD = 0x02;
    public const RESPONSE_KEY = 0x04;
    public const RESPONSE_KEY_PATH = 0x08;
    public const RESPONSE_CODE = 0x10;
    public const RESPONSE_REASON = 0x20;
    
    public const RESPONSE_DEBUG = 0x80;
    public const RESPONSE_ALL = 0xFF & ~self::RESPONSE_DEBUG;
    
    public const SEARCH_MESSAGES = 0x01;
    public const SEARCH_DIRS = 0x02;
    public const SEARCH_ALL = self::SEARCH_MESSAGES | self::SEARCH_DIRS;
    
    protected static $v2_headers = [
        "Content-Type" => "application/x-www-form-urlencoded",
        "Accept"       => "application/json",
    ];
    
    protected static $default_params = [
        "address"      => null,
        "api_address"  => null,
        "default_path" => "",
        "http_options" => [],
        "client"       => null,
        "proto"        => "http",
        "server"       => null,
        "port"         => 2379,
        "username"     => null,
        "password"     => null,
        "api"          => "v2",
        "response"     => self::RESPONSE_ALL,
    ];
    
    protected static $params = null;
    
    /**
     *  Return new class instance
     *
     * @param array|null $params             Initialization data, see init()
     * @param bool|null  $use_default_params [optional] [false] Create class based on default parameters
     *
     * @see AbstractEtcdClient::init()
     *
     * @return \vsitnikov\Etcd\AbstractEtcdClient
     */
    public static function new(?array $params = [], ?bool $use_default_params = false): AbstractEtcdClient
    {
        $params = array_merge($use_default_params ? self::$default_params : static::$params ?? [], $params ?? []);
        $new_class_type = "etcdTmpClass".uniqid();
        return eval("class {$new_class_type} extends ".__CLASS__." { protected static \$params = null; }; return new {$new_class_type}(\$params);//{$params}");
    }
    
    /**
     * Class constructor, all data passed to init method
     *
     * @param mixed ...$args
     *
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public function __construct(...$args)
    {
        self::init(...$args);
    }
    
    /**
     *  Class initialization
     *
     * @param array $params
     * <pre>
     * Initialization data
     * $server   ETCD server address
     * $port     [optional] etcd server port
     * $username [optional] etcd login
     * $password [optional] etcd password
     * $proto    [optional] connection protocol
     * $api      [optional] etcd api version
     * </pre>
     *
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function init(?array $params = [])
    {
        if (is_null(static::$params))
            static::$params = self::$default_params;
        
        $params = self::checkInitParams($params);
        $params['address'] = "{$params['proto']}://{$params['server']}:{$params['port']}";
        $params['api_address'] = "{$params['address']}/{$params['api']}";
        self::setParams($params);
    }
    
    /**
     *  Check initialization parameters
     *
     * @param array $params Input params
     * @param bool  $silent [optional] Don't throw exception, default true
     *
     * @return array|false false on fail or array, contains all class properties (internal and getting)
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    protected static function checkInitParams(array $params, bool $silent = false)
    {
        //  Fill the ONLY EXISTS parameters with the getting values
        $local_vars = self::getParams();
        foreach ($params as $key => $value)
            if (array_key_exists($key, $local_vars))
                $local_vars[$key] = $value;
        
        //  Check getting parameters
        if (is_null($local_vars['server']))
            if (!$silent)
                throw new EtcdInitializationException('"server" must be specified');
            else
                return false;
        if (!is_numeric($local_vars['port']) or $local_vars['port'] < 1 || $local_vars['port'] > 65534)
            if (!$silent)
                throw new EtcdInitializationException('"port" must be number in range 1-65534');
            else
                return false;
        if (!in_array($local_vars['proto'], ['http', 'https']))
            if (!$silent)
                throw new EtcdInitializationException('"proto" must be "https" or "http" only');
            else
                return false;
        if (!in_array($local_vars['api'], ['v2', 'v3']))
            if (!$silent)
                throw new EtcdInitializationException('"api" must be "v2" or "v3" only');
            else
                return false;
        return $local_vars;
    }
    
    /**
     *  Connect to ETCD
     *
     * @param array|null $options    [optional] Additional connection options
     * @param bool|null  $persistent [optional] [false] Store additional options for default connections
     *
     * @return \GuzzleHttp\Client
     */
    private static function getClient(?array $options = null, ?bool $persistent = false): Client
    {
        //  Return existing client, if no more additional options
        if (is_null($options) && self::issetParam('client'))
            return self::getParam("client");
        
        //  Set additional options if needed
        $http_options = self::getParam("http_options");
        if (is_array($options))
            $http_options = array_merge($http_options, $options);
        $http_options['timeout'] = $http_options['timeout'] ?? self::HTTP_TIMEOUT;
        
        $client = new Client([
            'headers' => array_diff($http_options, [null]),
        ]);
        return !$persistent ? $client : self::setParam('client', $client);
    }
    
    /**
     *  Get parameter by name
     *
     * @param string $name Name of parameter
     *
     * @return mixed|null Parameter value or null if parameter not exists
     */
    protected static function getParam(string $name)
    {
        if (is_array(static::$params) && array_key_exists($name, static::$params ?? []))
            return static::$params[$name];
        return null;
    }
    
    /**
     *  Get array of all parameters
     *
     * @return array
     */
    public static function getParams(): array
    {
        return static::$params ?? [];
    }
    
    /**
     * Set parameter value
     *
     * @param string $name  Parameter name
     * @param mixed  $value Parameter value
     *
     * @return mixed
     */
    protected static function setParam(string $name, $value)
    {
        return static::$params[$name] = $value;
    }
    
    /**
     *  Store all parameters
     *
     * @param array $value Array of all parameters
     *
     * @return array
     */
    protected static function setParams(array $value): array
    {
        return static::$params = $value;
    }
    
    /**
     *  Check of parameter exists by name
     *
     * @param string $name Name of parameter
     *
     * @return bool
     */
    protected static function issetParam(string $name): bool
    {
        return isset(static::$params[$name]);
    }
    
    /**
     * Delete parameter by name
     *
     * @param string $name Name of parameter
     */
    protected static function unsetParam(string $name): void
    {
        unset(static::$params[$name]);
    }
    
    /**
     *  setDefaultPath
     *
     * @param string $path Default path
     *
     * @return mixed Value
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function setDefaultPath($path)
    {
        self::checkInitParams([]);
        return self::setParam('default_path', trim($path, "/"));
    }
    
    /**
     *  Request to ETCD
     *
     * @param array|null $request_params
     * <pre>
     * Request parameters:
     * string type         Type request (POST, GET, PUT, etc...). Actual for v2 protocol
     * string url          [optional] ETCD api address, may be complete or relative, see "Generate URL address" below
     * array  payload      [optional] Payload
     * array  http_options [optional] HTTP client parameters
     * string service      [optional] Service (v3 only)
     * array  params       [optional] Initialization parameters
     * </pre>
     *
     * @return array
     * <pre>
     * Result array. Format:
     * bool         ['result']  Result operation (true if success, false if failure)
     * string       ['raw']     Raw body, depending on the constant self::DEFAULT_RESPONSE_RAW
     * array|object ['payload'] Payload response, must be an object or array, depending on the constant self::DEFAULT_RESPONSE_ARRAY
     * int          ['code']    Response code, depending on the constant self::DEFAULT_RESPONSE_CODE
     * string       ['reason']  Message of response code, depending on the constant self::DEFAULT_RESPONSE_REASON
     * string       ['debug']   Debug information, depending on the constant self::DEFAULT_RESPONSE_DEBUG
     * array        ['params']  Current query parameters
     * </pre>
     */
    protected static function request(array $request_params = [])
    {
        $type = $request_params['type'] ?? "POST";
        $url = $request_params['url'];
        $payload = $request_params['payload'];
        $http_options = $request_params['http_options'];
        $params = array_merge(self::getParams(), $request_params['params'] ?? []);
        $service = $request_params['service'] ?? ($params['api'] == "v2" ? "keys" : "kv");
        $client = self::getClient($http_options);
        
        if ($params['api'] == "v2") {
            $data = [
                'form_params' => $payload,
                'headers'     => self::$v2_headers,
            ];
            $service = "keys";
        }
        else {
            $type = "POST";
            $data = [
                'json'    => $payload,
                'headers' => [],
            ];
        }
        
        //  Generate URL address:
        //  - default api address if $url not set
        //  - $url as address if url is fill (begin from http)
        //  - add $url to default path if default path exists and url not begin from slash
        //  - add $url to default api address if other
        //  
        if (is_null($url) || $url[0] == "/")
            $url = $params['api_address']."/{$service}".($url ?? "");
        else if (substr($url, 0, 4) != "http")
            $url = $params['default_path'] != "" ? "{$params['api_address']}/{$service}/{$params['default_path']}/{$url}" : "{$params['api_address']}/keys/{$url}";
        
        if ($params['response'] & self::RESPONSE_DEBUG) {
            $debug = fopen("php://temp", "r+");
            $data['debug'] = $debug;
        }
        else
            $debug = false;
        
        //  Request
        $result = ['params' => $params];
        $body = $response = null;
        try {
            $response = $client->request($type, $url, $data);
            $body = $response->getBody()->getContents();
        } catch (GuzzleException $e) {
            $result = ["result" => false];
            if ($e instanceof RequestException && $e->hasResponse()) {
                $result['code'] = (int)$e->getResponse()->getStatusCode();
                $result['reason'] = $e->getResponse()->getReasonPhrase();
            }
        } finally {
            if (false !== $debug) {
                rewind($debug);
                $result['debug'] = stream_get_contents($debug);
                fclose($debug);
            }
            if (false === $result['result'])
                return $result;
        }
        
        //  Prepare response
        $result['raw'] = $body;
        
        $result['payload'] = json_decode($body, true, 512, JSON_BIGINT_AS_STRING);
        $result['result'] = json_last_error() == JSON_ERROR_NONE;
        
        if ($result['result'] && isset($result['payload']['errorCode'])) {
            $result['code'] = (int)$result['payload']['errorCode'];
            $result['reason'] = $result['payload']['message'];
        }
        else {
            $result['code'] = (int)$response->getStatusCode();
            $result['reason'] = $response->getReasonPhrase();
            $result['key_path'] = $result['payload']['node']['key'];
            preg_match('/.*\/(?<key>.*)$/', $result['key_path'], $tmp);
            $result['key'] = $tmp['key'];
        }
        return $result;
    }
    
    /**
     *  Check ETCD connect
     *
     * @param string|null $address [optional] HealthCheck path for server
     * @param array|null  $params  [optional] Initialization parameters
     *
     * @return bool
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function healthCheck(?string $address = null, ?array $params = null): bool
    {
        $etcd_params = self::checkInitParams($params ?? []);
        if (is_null($address))
            $address = $etcd_params['address']."/health";
        else
            $address = $etcd_params['address']."/{$address}";
        return self::request(['type' => "GET", 'url' => $address])['code'] == 200;
    }
    
    /**
     *  Setting/Changing value of a key
     *
     * @param string     $key    Key path
     * @param string     $value  Key value
     * @param int        $ttl    [optional] [unlimited] Time to live value (in seconds), 0 is unlimited
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function set(string $key, string $value, int $ttl = 0, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $payload = ["value" => $value];
        if (is_numeric($ttl) && $ttl > 0)
            $payload['ttl'] = $ttl;
        $result = self::request(['type' => "PUT", 'url' => $key, 'payload' => $payload, 'params' => $params]);
        $result['result'] = in_array($result['code'], [200, 201]);
        return self::stripResponse($result);
    }
    
    /**
     *  Get value of a key
     *
     * @param string     $key    Key
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array + ['value'] - key value
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function get(string $key, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $result = self::request(['type' => "GET", 'url' => $key, 'params' => $params]);
        $result['result'] = $result['code'] == 200;
        if ($result['result']) {
            $result['value'] = $result['payload']['node']['value'];
            if (isset($result['payload']['node']['dir']))
                $result['dir'] = true;
            if (isset($result['payload']['node']['ttl']))
                $result['ttl'] = $result['payload']['node']['ttl'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Set value if key does not exist
     *
     * @param string     $key    Key path
     * @param string     $value  Key value
     * @param int        $ttl    [optional] [0] Time to live value (in seconds), 0 is unlimited
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function lock(string $key, string $value, int $ttl = 0, ?array $params = null)
    {
        self::checkInitParams($params ?? []);
        $key .= "?prevExist=false";
        $payload = ["value" => $value];
        if (is_numeric($ttl) && $ttl > 0)
            $payload['ttl'] = $ttl;
        $result = self::request(['type' => "PUT", 'url' => $key, 'payload' => $payload, 'params' => $params]);
        $result['result'] = $result['code'] == 201;
        return self::stripResponse($result);
    }
    
    /**
     *  Delete value of a key
     *
     * @param string     $key    Path to deleting key
     * @param array|null $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function delete(string $key, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $result = self::request(['type' => "DELETE", 'url' => $key, 'params' => $params]);
        $result['result'] = $result['code'] == 200;
        return self::stripResponse($result);
    }
    
    /**
     *  Create dir (v2 protocol only)
     *
     * @param string|null $key    Path to new directory
     * @param int         $ttl    [optional] [unlimited] Time to live value (in seconds), 0 is unlimited
     * @param array|null  $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function createDir(?string $key, int $ttl = 0, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $payload = ["dir" => true];
        if (is_numeric($ttl) && $ttl > 0)
            $payload['ttl'] = $ttl;
        $result = self::request(['type' => "PUT", 'url' => $key ?? "", 'payload' => $payload, 'params' => $params]);
        $result['result'] = in_array($result['code'], [200, 201]);
        return self::stripResponse($result);
    }
    
    /**
     *  Check if the path is a directory (v2 protocol only)
     *
     * @param string|null $key    Checking path
     * @param array|null  $params [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function isDir(?string $key, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        $result = self::request(['type' => "GET", 'url' => $key ?? "", 'params' => $params]);
        $result['result'] = $result['code'] == 200 && isset($result['payload']['node']['dir']);
        return self::stripResponse($result);
    }
    
    /**
     *  Read value from directory with recursive option (default on)
     *
     * @param string     $dir_path  Path to the directory from which the value will be read
     * @param bool|null  $recursive [optional] [true] Recursive read
     * @param array|null $params    [optional] Initialization parameters
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readDirRecursive(string $dir_path, ?bool $recursive = null, ?array $params = null)
    {
        self::checkInitParams($params ?? []);
        if ($recursive ?? true)
            $dir_path .= "?recursive=true&sorted=true";
        else
            $dir_path .= "?sorted=true";
        $result = self::request(['type' => "GET", 'url' => $dir_path, 'params' => $params]);
        $result['result'] = $result['code'] == 200;
        if (is_array($result['payload']['node']['nodes'])) {
            $result['value'] = $result['payload']['node']['nodes'];
        }
        return self::stripResponse($result);
    }
    
    /**
     *  Read value from directory
     *
     * @param string     $dir_path Path to the directory from which the value will be read
     * @param array|null $params   [optional] Initialization parameters
     *
     * @see AbstractEtcdClient::readDirRecursive()
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readDir(string $dir_path, ?array $params = null)
    {
        return self::readDirRecursive($dir_path, false, $params);
    }
    
    /**
     *  Read value from dir by regex key filter with recursive option (default on)
     *
     * @param string     $dir_path  Path to the directory from which the value will be read
     * @param string     $regex     Key
     * @param int|null   $options   [optional] Search options (default: self::SEARCH_ALL):<br>
     *                              self::SEARCH_MESSAGES - search in message keys,<br>
     *                              self::SEARCH_DIRS     - search in dir keys
     * @param bool|null  $recursive [optional] [true] Recursive read
     * @param array|null $params    [optional] Initialization parameters
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readDirRegexKeyRecursive(string $dir_path, string $regex, ?int $options = null, ?bool $recursive = null, ?array $params = null)
    {
        $result = self::readDirRecursive($dir_path, $recursive, $params);
        $result['result'] = $result['code'] == 200;
        if (is_array($result['payload']['node']['nodes']))
            $result['value'] = $result['payload'] = self::recursiveKeySearch($result['payload']['node']['nodes'], $regex, $options) ?? [];
        else
            $result['value'] = $result['payload'] = $result['value'] ?? [];
        return self::stripResponse($result);
    }
    
    /**
     *  Read value from dir by regex key filter
     *
     * @param string     $dir_path Path to the directory from which the value will be read
     * @param string     $regex    Key
     * @param int|null   $options  [optional] Search options (default: self::SEARCH_ALL):<br>
     *                             self::SEARCH_MESSAGES - search in message keys,<br>
     *                             self::SEARCH_DIRS     - search in dir keys
     * @param array|null $params   [optional] Initialization parameters
     *
     * @see AbstractEtcdClient::readDirRegexKeyRecursive()
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readDirRegexKey(string $dir_path, string $regex, ?int $options = null, ?array $params = null)
    {
        return self::readDirRegexKeyRecursive($dir_path, $regex, $options, false, $params);
    }
    
    /**
     *  Read value from dir by regex value filter with recursive option (default on)
     *
     * @param string     $dir_path  Path to the directory from which the value will be read
     * @param string     $regex     Key
     * @param bool|null  $recursive [optional] [true] Recursive read
     * @param array|null $params    [optional] Initialization parameters
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readDirRegexValueRecursive(string $dir_path, string $regex, ?bool $recursive = null, ?array $params = null)
    {
        $result = self::readDirRecursive($dir_path, $recursive, $params);
        $result['result'] = $result['code'] == 200;
        if (is_array($result['payload']['node']['nodes']))
            $result['value'] = $result['payload'] = self::recursiveValueSearch($result['payload']['node']['nodes'], $regex) ?? [];
        else
            $result['value'] = $result['payload'] = $result['value'] ?? [];
        return self::stripResponse($result);
    }
    
    /**
     *  Read value from dir by regex value filter
     *
     * @param string     $dir_path Path to the directory from which the value will be read
     * @param string     $regex    Key
     * @param array|null $params   [optional] Initialization parameters
     *
     * @see AbstractEtcdClient::readDirRegexValueRecursive()
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readDirRegexValue(string $dir_path, string $regex, ?array $params = null)
    {
        return self::readDirRegexValueRecursive($dir_path, $regex, false, $params);
    }
    
    /**
     *  Delete dir
     *
     * @param string|null $dir_path Path to deleting directory
     * @param bool        $force    [optional] [false] Delete non empty directory
     * @param array|null  $params   [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function deleteDir(?string $dir_path, bool $force = false, ?array $params = null): array
    {
        self::checkInitParams($params ?? []);
        if ($force)
            $dir_path .= "?recursive=true";
        else
            $dir_path .= "?dir=true";
        $result = self::request(['type' => "DELETE", 'url' => $dir_path ?? "", 'params' => $params]);
        $result['result'] = $result['code'] == 200;
        return self::stripResponse($result);
    }
    
    /**
     *  Set value into queue
     *
     * @param string     $queue_path Path to the directory in which the queue will be created
     * @param string     $value      Key value
     * @param int        $ttl        [optional] [0] Time to live value (in seconds), 0 is unlimited
     * @param array|null $params     [optional] Initialization parameters
     *
     * @return array Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function addQueue(string $queue_path, string $value, int $ttl = 0, ?array $params = null)
    {
        $result = self::isDir($queue_path, $params);
        if (!$result['result'])
            return self::stripResponse(["code" => 104, "reason" => "Not a directory", "result" => false]);
        $payload = ["value" => $value];
        if (is_numeric($ttl) && $ttl > 0)
            $payload['ttl'] = $ttl;
        $result = self::request(['type' => "POST", 'url' => $queue_path, 'payload' => $payload, 'params' => $params]);
        $result['result'] = $result['code'] == 201;
        return self::stripResponse($result);
    }
    
    /**
     *  Read value from queue
     *
     * @param string     $queue_path Path to the queue directory from which the value will be read
     * @param array|null $params     [optional] Initialization parameters
     *
     * @see AbstractEtcdClient::readDir()
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readQueue(string $queue_path, ?array $params = null)
    {
        return self::readDir($queue_path, $params);
    }
    
    /**
     *  Read value from queue by key
     *
     * @param string     $queue_path Path to the queue directory from which the value will be read
     * @param string     $regex      Key
     * @param array|null $params     [optional] Initialization parameters
     *
     * @see AbstractEtcdClient::readDirRegexValue()
     *
     * @return array Result array + ['value'] - $result['payload']['node']['nodes']
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function readQueueRegexValue(string $queue_path, string $regex, ?array $params = null)
    {
        return self::readDirRegexValue($queue_path, $regex, $params);
    }
    
    /**
     *  Get first value from queue
     *
     * @param string     $queue_path Path to queue dir
     * @param array|null $params     [optional] Initialization parameters
     *
     * @return array|bool Result array or false when fail
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function getQueueFirst(string $queue_path, ?array $params = null)
    {
        $queue = self::readDir($queue_path, $params)['value'];
        if (!is_array($queue) || !sizeof($queue))
            return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
        for ($i = 0; $i < sizeof($queue); $i++) {
            if (!preg_match('/.*\/(?<key>.*)$/', $queue[$i]['key'], $key))
                continue;
            $key = $key['key'];
            $response = self::getQueueLock($queue_path, $key, $params);
            if ($response !== false)
                return $response;
        }
        return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
    }
    
    /**
     *  Get last value from queue
     *
     * @param string     $queue_path Path to queue dir
     * @param array|null $params     [optional] Initialization parameters
     *
     * @return array|bool Result array or false when fail
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function getQueueLast(string $queue_path, ?array $params = null)
    {
        $queue = self::readDir($queue_path, $params)['value'];
        if (!is_array($queue) || !sizeof($queue))
            return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
        for ($i = sizeof($queue) - 1; $i >= 0; $i--) {
            if (!preg_match('/.*\/(?<key>.*)$/', $queue[$i]['key'], $key))
                continue;
            $key = $key['key'];
            $response = self::getQueueLock($queue_path, $key, $params);
            if ($response !== false)
                return $response;
        }
        return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
    }
    
    /**
     *  Get first value from queue by regex value filter
     *
     * @param string     $queue_path Path to queue dir
     * @param string     $regex      Key
     * @param array|null $params     [optional] Initialization parameters
     *
     * @return array|bool Result array or false when fail
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function getQueueRegexValueFirst(string $queue_path, string $regex, ?array $params = null)
    {
        $queue = self::readDirRegexValue($queue_path, $regex, $params)['value'];
        if (!is_array($queue) || !sizeof($queue))
            return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
        for ($i = 0; $i < sizeof($queue); $i++) {
            if (!preg_match('/.*\/(?<key>.*)$/', $queue[$i]['key'], $key))
                continue;
            $key = $key['key'];
            $response = self::getQueueLock($queue_path, $key, $params);
            if ($response !== false)
                return $response;
        }
        return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
    }
    
    /**
     *  Get last value from queue by regex value filter
     *
     * @param string     $queue_path Path to queue dir
     * @param string     $regex      Key
     * @param array|null $params     [optional] Initialization parameters
     *
     * @return array|bool Result array or false when fail
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public static function getQueueRegexValueLast(string $queue_path, string $regex, ?array $params = null)
    {
        $queue = self::readDirRegexValue($queue_path, $regex, $params)['value'];
        if (!is_array($queue) || !sizeof($queue))
            return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
        for ($i = sizeof($queue) - 1; $i >= 0; $i--) {
            if (!preg_match('/.*\/(?<key>.*)$/', $queue[$i]['key'], $key))
                continue;
            $key = $key['key'];
            $response = self::getQueueLock($queue_path, $key, $params);
            if ($response !== false)
                return $response;
        }
        return self::stripResponse(["code" => 204, "reason" => "Queue is empty", "result" => false]);
    }
    
    /**
     *  Read key from queue
     *
     * @param string     $queue_path Path to queue dir
     * @param string     $key        Key name
     * @param array|null $params     [optional] Initialization parameters
     *
     * @return array|bool Result array
     * @throws \vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    protected static function getQueueLock(string $queue_path, string $key, ?array $params = null)
    {
        $result = self::isDir("{$queue_path}/_queue_locks", ['response' => self::RESPONSE_CODE]);
        if (!$result['result']) {
            if ($result['code'] == 404)
                self::createDir("{$queue_path}/_queue_locks");
            else
                return false;
        }
        
        //  Create lock on 1 second
        if (self::lock("{$queue_path}/_queue_locks/{$key}", "locked", 1)['result']) {
            $response = self::get("{$queue_path}/{$key}", $params);
            self::delete("{$queue_path}/_queue_locks/{$key}");
            if ($response['result']) {
                self::delete("{$queue_path}/{$key}");
                return $response;
            }
            else
                return false;
        }
        return false;
    }
    
    /**
     *  Convert object to array
     *
     * @param object $input Object for convert
     *
     * @return array|bool|float|int|string
     */
    protected static function object2array($input)
    {
        return is_scalar($input) ? $input : array_map("self::".__FUNCTION__, (array)$input);
    }
    
    /**
     *  Cut output array
     *
     * @param array $response Response with query settings
     *
     * @return array Result array - cut fields
     */
    protected static function stripResponse(array $response): array
    {
        if (!isset($response['params']['response']))
            return $response;
        $exclude = ['params'];
        if (!($response['params']['response'] & self::RESPONSE_RAW))
            $exclude[] = 'raw';
        if (!($response['params']['response'] & self::RESPONSE_PAYLOAD))
            $exclude[] = 'payload';
        if (!($response['params']['response'] & self::RESPONSE_KEY))
            $exclude[] = 'key';
        if (!($response['params']['response'] & self::RESPONSE_KEY_PATH))
            $exclude[] = 'key_path';
        if (!($response['params']['response'] & self::RESPONSE_CODE))
            $exclude[] = 'code';
        if (!($response['params']['response'] & self::RESPONSE_REASON))
            $exclude[] = 'reason';
        if (!($response['params']['response'] & self::RESPONSE_DEBUG))
            $exclude[] = 'debug';
        return array_diff_key($response, array_flip($exclude));
    }
    
    /**
     *  Recursive search in nodes by key
     *
     * @param array    $nodes   Node
     * @param string   $regex   Regex
     * @param int|null $options [optional] Search options (default: self::SEARCH_ALL):<br>
     *                          self::SEARCH_MESSAGES - search in message keys,<br>
     *                          self::SEARCH_DIRS     - search in dir keys
     *
     * @return array|null
     */
    protected static function recursiveKeySearch(array $nodes, string $regex, ?int $options = null)
    {
        $options = $options ?? self::SEARCH_ALL;
        foreach ($nodes as $index => $node) {
            if ($node['dir'] ?? false && is_array($node['nodes'])) {
                if (is_null($nodes[$index]['nodes'] = self::recursiveKeySearch($node['nodes'] ?? [], $regex, $options)))
                    if ($options & self::SEARCH_DIRS) {
                        if (!preg_match($regex, substr($node['key'], strrpos($node['key'], "/") + 1)))
                            $nodes[$index] = null;
                    }
                    else
                        $nodes[$index] = null;
            }
            elseif (isset($node['key'])) {
                if ($options & self::SEARCH_MESSAGES) {
                    if (!preg_match($regex, substr($node['key'], strrpos($node['key'], "/") + 1)))
                        $nodes[$index] = null;
                }
                else
                    $nodes[$index] = null;
            }
        }
        $nodes = array_diff($nodes, [null]);
        return sizeof($nodes) ? array_values($nodes) : null;
    }
    
    /**
     *  Recursive search in nodes by value
     *
     * @param array  $nodes Node
     * @param string $regex Regex
     *
     * @return array|null
     */
    protected static function recursiveValueSearch(array $nodes, string $regex)
    {
        foreach ($nodes as $index => $node) {
            if ($node['dir'] ?? false && is_array($node['nodes'])) {
                if (is_null($nodes[$index]['nodes'] = self::recursiveValueSearch($node['nodes'] ?? [], $regex)))
                    $nodes[$index] = null;
            }
            elseif (isset($node['value']) && !preg_match($regex, $node['value'])) {
                $nodes[$index] = null;
            }
        }
        $nodes = array_diff($nodes, [null]);
        return sizeof($nodes) ? array_values($nodes) : null;
    }
}
