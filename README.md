####Attention! This project is in development, do not use it, everything can change completely.

##Create class
#####Initialization parameters
`$params = [
    "server" => "server.ru",
    "port"   => 2500,
    "api"    => "v2",
];`

#####Initialize abstract class
`etcd::init($params);`

#####Create new class identical on abstract class settings
`$etc_class1 = etcd::new();`

#####Modify settings abstract class
`etcd::init(['port' => 2700]);`

###Multiple instances
#####Create new class with new settings, based on abstract class settings
`$etc_class2 = etcd::new(['server' => "newserver.ru"], true);`

#####Create new class with new settings, based on $etc_class1 settings
`$etc_class3 = $etc_class1::new(['server' => "oldserver.ru"]);`

or

`$etc_class3 = $etc_class1->new(['server' => "oldserver.ru"]);`

###Usage
#####Set default path
`etcd::setDefaultPath("/ps/data/apps/mondb/unit_test");`

#####Get data from absolute path (path begin from http)
`$result = etcd::get("http://localhost:2376/v2/keys/data/app/unit_test/test_data");`

#####Get data from full path (path begin from slash)
`$result = etcd::get("/data/app/unit_test/test_data");`

#####Get data from relative (for default path or full path if default path not exists) path (path NOT begin from slash or http)
`$result = etcd::get("test_data");`

##### Get data with personal query settings
`$result = etcd::get("test", ['response' => etcd::RESPONSE_CODE | etcd::RESPONSE_DEBUG]);`

