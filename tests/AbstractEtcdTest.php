<?php declare(strict_types=1);

/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

use PHPUnit\Framework\TestCase;
use vsitnikov\Etcd\AbstractEtcdClient as etcd;
use vsitnikov\Etcd\Exceptions\EtcdInitializationException;

/**
 * ETCD class
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   vsitnikov\Etcd
 * @copyright Copyright (c) 2019
 */
final class AbstractEtcdTest extends TestCase
{
    /**
     * ETCD will throw initialization exception
     *
     * @expectedException vsitnikov\Etcd\Exceptions\EtcdInitializationException
     */
    public function testBeforeInitialization()
    {
        etcd::get("dummy");
    }
    
    /**
     * Tests Initialization
     */
    public function testInitialization()
    {
        //  Import the initialized parameters and add them with a deliberately false parameter
        global $global_options;
        $global_options['dummy'] = "foo";
        $global_options['response'] = ($global_options['response'] ?? etcd::RESPONSE_NONE) | etcd::RESPONSE_PAYLOAD | etcd::RESPONSE_CODE;
        
        //  Initialize an abstract class, there should be no false parameter
        etcd::init($global_options);
        $this->assertArrayNotHasKey("dummy", etcd::getParams());
        $this->assertEquals($global_options['server'], etcd::getParams()['server']);
    }
    
    /**
     * Test new instances
     */
    public function testCheckCreateNewInstances()
    {
        $this->assertInstanceOf(etcd::class, $etcdClass1 = etcd::new());
        $this->assertEquals(etcd::getParams(), $etcdClass1::getParams());
        $etcdClass1::init(["server" => "dummy"]);
        $this->assertNotEquals(etcd::getParams(), $etcdClass1::getParams());
        $etcdClass2 = etcd::new(["server" => "dummy"]);
        $this->assertEquals($etcdClass1::getParams(), $etcdClass2::getParams());
        $etcdClass2::init(["port" => "1"]);
        $this->assertNotEquals($etcdClass1::getParams(), $etcdClass2::getParams());
    }
    
    /**
     * Test create dirs
     */
    public function testWorkWithDirs()
    {
        //  Delete default path (sandbox)
        etcd::deleteDir(null, true);
        $this->assertFalse(etcd::isDir(null)['result']);
        
        //  Create sandbox
        $this->assertTrue(etcd::createDir(null)['result']);
        $this->assertTrue(etcd::isDir(null)['result']);
        
        //  Create dir in sandbox with TTL = 1 second and check it
        $result = etcd::createDir("test_ttl", 1);
        $this->assertTrue($result['result']);
        $this->assertEquals(1, $result['payload']['node']['ttl']);
        $this->assertTrue(etcd::isDir("test_ttl")['result']);
        
        //  Check TTL dir after TTL expired
        sleep(2);
        $this->assertFalse(etcd::isDir("test_ttl")['result']);
        
        //  Create normal dir
        $result = etcd::createDir("data");
        $this->assertTrue($result['result']);
        $this->assertArrayNotHasKey("ttl", $result['payload']['node']);
    }
    
    /**
     * Test use queue
     */
    public function testWorkWithDirRecursive()
    {
        
        //  Create dir with subdir (one pre created and two automatic created) and push 4 different values
        $this->assertTrue(etcd::createDir("data/test_dir_recursive")['result']);
        $this->assertTrue(etcd::createDir("data/test_dir_recursive/subdir1")['result']);
        $this->assertTrue(etcd::set("data/test_dir_recursive/dataRegexp", "valueRegex")['result']);
        $this->assertTrue(etcd::set("data/test_dir_recursive/subdir1/data1Regexp", "value1Regexp")['result']);
        $this->assertTrue(etcd::set("data/test_dir_recursive/subdir2/data2Regexp", "value2Regexp")['result']);
        $this->assertTrue(etcd::set("data/test_dir_recursive/subdir2/subdir2.2/data2.2Regexp", "value2.2Regexp")['result']);
        $this->assertTrue(etcd::set("data/test_dir_recursive/subdir3/data3Regexp", "value3Regexp")['result']);
        
        //  Read dir recursive
        $list = etcd::readDirRecursive("data/test_dir_recursive")['value'];
        $this->assertArrayNotHasKey("nodes", $list[0]);
        $this->assertIsArray($list[1]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[1]['nodes'][0]);
        $this->assertIsArray($list[2]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[2]['nodes'][0]);
        $this->assertIsArray($list[2]['nodes'][1]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[2]['nodes'][1]['nodes'][0]);
        
        //  Read dir recursive with regexp key filter (ignore 2 keys)
        $list = etcd::readDirRegexKeyRecursive("data/test_dir_recursive", "/data\d{1}Regexp/", etcd::SEARCH_ALL)['value'];
        $this->assertArrayHasKey("nodes", $list[0]);
        $this->assertIsArray($list[1]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[1]['nodes'][0]);
        $this->assertIsArray($list[2]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[2]['nodes'][0]);
        $this->assertNull($list[2]['nodes'][1]);
        
        //  Read dir recursive with regexp key filter (only dir keys)
        $list = etcd::readDirRegexKeyRecursive("data/test_dir_recursive", "/data\d{1}Regexp/", etcd::SEARCH_DIRS)['value'];
        $this->assertEquals(0, sizeof($list));
        
        //  Read dir recursive with regexp key filter (only dir keys)
        $list = etcd::readDirRegexKeyRecursive("data/test_dir_recursive", "/sub/", etcd::SEARCH_DIRS)['value'];
        $this->assertEquals(3, sizeof($list));
        $this->assertArrayHasKey("nodes", $list[0]);
        $this->assertEquals(0, $list[0]['nodes']);
        $this->assertIsArray($list[1]['nodes']);
        $this->assertEquals(1, sizeof($list[1]['nodes']));
        $this->assertArrayHasKey("nodes", $list[2]);
        $this->assertEquals(0, $list[2]['nodes']);
        
        //  Read dir recursive with regexp key filter (only message keys)
        $list = etcd::readDirRegexKeyRecursive("data/test_dir_recursive", "/data\d{1}Regexp/", etcd::SEARCH_MESSAGES)['value'];
        $this->assertIsArray($list[0]['nodes']);
        $this->assertEquals(1, sizeof($list[0]['nodes']));
        $this->assertIsArray($list[1]['nodes']);
        $this->assertEquals(1, sizeof($list[1]['nodes']));
        $this->assertIsArray($list[2]['nodes']);
        $this->assertEquals(1, sizeof($list[2]['nodes']));
        
        //  Read dir recursive with regexp key filter (only message keys)
        $list = etcd::readDirRegexKeyRecursive("data/test_dir_recursive", "/sub/", etcd::SEARCH_MESSAGES)['value'];
        $this->assertEquals(0, sizeof($list));
        
        //  Read dir recursive with regexp key filter (only 1 key)
        $list = etcd::readDirRegexKeyRecursive("data/test_dir_recursive", "/data\d+\.\d+Regexp/", etcd::SEARCH_ALL)['value'];
        $this->assertArrayHasKey("nodes", $list[0]);
        $this->assertIsArray($list[0]['nodes']);
        $this->assertArrayHasKey("nodes", $list[0]['nodes'][0]);
        $this->assertIsArray($list[0]['nodes'][0]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[0]['nodes'][0]['nodes'][0]);
        
        //  Read dir recursive with regexp value filter (ignore 2 keys)
        $list = etcd::readDirRegexValueRecursive("data/test_dir_recursive", "/value\d{1}Regexp/")['value'];
        $this->assertArrayHasKey("nodes", $list[0]);
        $this->assertIsArray($list[1]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[1]['nodes'][0]);
        $this->assertIsArray($list[2]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[2]['nodes'][0]);
        $this->assertNull($list[2]['nodes'][1]);
        
        //  Read dir recursive with regexp value filter (only 1 key)
        $list = etcd::readDirRegexValueRecursive("data/test_dir_recursive", "/value\d+\.\d+Regexp/")['value'];
        $this->assertArrayHasKey("nodes", $list[0]);
        $this->assertIsArray($list[0]['nodes']);
        $this->assertArrayHasKey("nodes", $list[0]['nodes'][0]);
        $this->assertIsArray($list[0]['nodes'][0]['nodes']);
        $this->assertArrayNotHasKey("nodes", $list[0]['nodes'][0]['nodes'][0]);
        
        //  Read dir
        $list = etcd::readDir("data/test_dir_recursive")['value'];
        $this->assertEquals(4, sizeof($list ?? []));
        $values = [false, true, true, true];
        foreach ($list as $key => $value)
            $this->assertEquals($values[$key], $value['dir']);
        
        //  Read dir with regexp key/value filters
        $list = etcd::readDirRegexKey("data/test_dir_recursive", "/data.*/")['value'];
        $this->assertEquals(1, sizeof($list));
        $list = etcd::readDirRegexKey("data/test_dir_recursive", "/.*/", etcd::SEARCH_ALL)['value'];
        $this->assertEquals(4, sizeof($list));
        $list = etcd::readDirRegexKey("data/test_dir_recursive", "/.*/", etcd::SEARCH_DIRS)['value'];
        $this->assertEquals(3, sizeof($list));
        $list = etcd::readDirRegexKey("data/test_dir_recursive", "/.*/", etcd::SEARCH_MESSAGES)['value'];
        $this->assertEquals(1, sizeof($list));
        $list = etcd::readDirRegexValue("data/test_dir_recursive", "/.*/")['value'];
        $this->assertEquals(1, sizeof($list));
        $list = etcd::readDirRegexValue("data/test_dir_recursive", "/uere/i")['value'];
        $this->assertEquals(1, sizeof($list));
        $list = etcd::readDirRegexValue("data/test_dir_recursive", "/uere/")['value'];
        $this->assertEquals(0, sizeof($list));
    }
    
    /**
     * Test read/write values
     */
    public function testWorksWithValues()
    {
        //  Create normal value
        $this->assertTrue(etcd::set("data/test_value", "test")['result']);
        
        //  Create TTL value in non existing dir (with dir automatic create)
        $this->assertTrue(etcd::set("data/tesl_ttl/test_value_ttl", "test_ttl", 1)['result']);
        
        //  Values is dir?
        $result = etcd::isDir("data/test_value");
        $this->assertFalse($result['result']);
        $this->assertEquals(200, $result['code']);
        $this->assertTrue(etcd::isDir("data/tesl_ttl")['result']);
        $this->assertFalse(etcd::isDir("data/tesl_ttl/test_value_ttl")['result']);
        
        //  Get TTL value an check it
        $result = etcd::get("data/tesl_ttl/test_value_ttl");
        $this->assertTrue($result['result']);
        $this->assertTrue($result['value'] == "test_ttl");
        $this->assertEquals(1, $result['payload']['node']['ttl']);
        
        //  Get normal value an check it
        $result = etcd::get("data/test_value");
        $this->assertTrue($result['result']);
        $this->assertTrue($result['value'] == "test");
        $this->assertArrayNotHasKey("ttl", $result['payload']['node']);
        
        //  Check TTL value after TTL expired (but dir not delete)
        sleep(2);
        $this->assertFalse(etcd::get("data/tesl_ttl/test_value_ttl")['result']);
        $this->assertTrue(etcd::isDir("data/tesl_ttl")['result']);
        
    }
    
    /**
     * Test use locks
     */
    public function testWorkWithLock()
    {
        //  Create lock
        $this->assertTrue(etcd::lock("data/test_lock", "lock", 1)['result']);
        $this->assertTrue(etcd::get("data/test_lock")['result']);
        
        //  Create lock again
        $this->assertFalse(etcd::lock("data/test_lock", "lock")['result']);
        
        //  And again
        sleep(2);
        $this->assertTrue(etcd::lock("data/test_lock", "lock")['result']);
    }
    
    /**
     * Test use queue
     */
    public function testWorkWithQueueDir()
    {
        
        //  Create queue and push 3 values (TTL on data2 is set)
        $this->assertTrue(etcd::createDir("data/test_queue")['result']);
        $this->assertTrue(etcd::addQueue("data/test_queue", "data1")['result']);
        $this->assertTrue(etcd::addQueue("data/test_queue", "data2", 1)['result']);
        $this->assertTrue(etcd::addQueue("data/test_queue", "data3")['result']);
        $this->assertTrue(etcd::addQueue("data/test_queue", "data4")['result']);
        
        //  Read queue
        $queue = etcd::readQueue("data/test_queue")['value'];
        $values = ["data1", "data2", "data3", "data4"];
        foreach ($queue as $key => $value)
            $this->assertEquals($values[$key], $value['value']);
        
        //  Wait and read again
        sleep(2);
        $queue = etcd::readQueue("data/test_queue")['value'];
        $values = ["data1", "data3", "data4", "value_not_exists"];
        foreach ($queue as $key => $value)
            $this->assertEquals($values[$key], $value['value']);
        
        //  Read from queue: first, last, no value, one value
        $this->assertEquals(2, sizeof(etcd::readQueueRegexValue("data/test_queue", "/data[34]/")['value']));
        $this->assertEquals(etcd::getQueueRegexValueFirst("data/test_queue", "/3/")['value'], "data3");
        $this->assertEquals(etcd::getQueueLast("data/test_queue")['value'], "data4");
        $this->assertEquals(etcd::getQueueFirst("data/test_queue")['value'], "data1");
        $this->assertEquals(etcd::getQueueFirst("data/test_queue", ['response' => etcd::RESPONSE_CODE])['code'], 204);
        $this->assertTrue(etcd::addQueue("data/test_queue", "data5")['result']);
        $this->assertEquals(etcd::getQueueLast("data/test_queue")['value'], "data5");
        $this->assertEquals(etcd::getQueueLast("data/test_queue", ['response' => etcd::RESPONSE_CODE])['code'], 204);
        $this->assertTrue(etcd::addQueue("data/test_queue", "data6")['result']);
        $this->assertNull(etcd::getQueueRegexValueLast("data/test_queue", "/7/")['value']);
        $this->assertEquals(etcd::getQueueRegexValueLast("data/test_queue", "/6/")['value'], "data6");
        $this->assertEquals(etcd::getQueueRegexValueLast("data/test_queue", "/6/", ['response' => etcd::RESPONSE_CODE])['code'], 204);
    }
    
    /**
     * Test work with errors
     */
    public function testWorkWithErrors()
    {
        //  Create already existing dir
        $this->assertFalse(etcd::createDir("data")['result']);
        
        //  Create dir in value
        $this->assertFalse(etcd::createDir("data/test_value/test_dir")['result']);
        
        //  Create value over dir
        $this->assertFalse(etcd::set("data", "dummy")['result']);
        
        //  Create dir in lock
        
        //  Create lock over dir
        
        //  Create queue in value
        $this->assertFalse(etcd::addQueue("data/test_value", "test_override_queue")['result']);
        
        //  Create value over queue
        $this->assertFalse(etcd::set("data/test_queue", "dummy")['result']);
        
        //  Check non exists dir
        $this->assertFalse(etcd::isDir("dir_not_exists")['result']);
        
        //  Get non exists value
        $this->assertFalse(etcd::get("data/value_not_exists")['result']);
    }
    
    /**
     * Test delete dir/values
     */
    public function testWorkWithDeleteData()
    {
        //  Delete non exists dir
        $this->assertFalse(etcd::deleteDir("dir_not_exists")['result']);
        
        //  Delete non empty dir
        $this->assertFalse(etcd::deleteDir("data")['result']);
        
        //  Delete non exists value
        $this->assertFalse(etcd::deleteDir("data/value_not_exists")['result']);
        
        //  Delete non empty dir with force
        $this->assertTrue(etcd::deleteDir(null, true)['result']);
    }
}
